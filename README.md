## Prerequisites
1. Setup the following environment variables:
- `S3_BUCKET_NAME`
- `S3_FILE_PREFIX`
- `DRY_RUN` (optional)

2. Run :
```
aws sso login
```

3. Have the AWS config current profile allowing to access to `S3_BUCKET_NAME`.

## Run
```
npm run start
```
