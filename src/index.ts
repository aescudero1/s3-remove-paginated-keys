import { S3Service } from '@kiloutou1/s3-service';
import 'dotenv/config'

const s3Service = new S3Service();

const bucketName = process.env.S3_BUCKET_NAME!;
const prefix = process.env.S3_FILE_PREFIX!;
const dryRun = process.env.DRY_RUN === 'true';

(async () => {
  const keys = await s3Service.getFileKeys(bucketName, prefix);
  const paginationKeys = keys.filter(key => key.match(/__page_\d+\.html/));

  for (const paginationKey of paginationKeys) {
    console.log(`Deleting file ${prefix}/${paginationKey} from ${bucketName}`);
    if (dryRun) {
      continue;
    }
    await s3Service.deleteFile(bucketName, paginationKey);
  }
})();
